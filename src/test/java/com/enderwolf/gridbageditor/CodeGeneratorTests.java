package com.enderwolf.gridbageditor;

import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;

import junit.framework.Assert;
import junit.framework.TestCase;

public class CodeGeneratorTests extends TestCase {
    Vector<LayoutEntry> entries = new Vector<>();

    public CodeGeneratorTests() {
        this.init();
    }

    public CodeGeneratorTests(String name) {
        super(name);
        this.init();
    }

    /**
     * 'javac' does not exist on $PATH under Windows, so asserting that the code
     * actually compiles on all OS's is currently not possible.
     *
     * It is assumed that javac exists on the $PATH under all non-Windows OS's.
     * Under Windows, the code is printed to STDOUT for manual review.
     */
    public void testCodeCompilation() {
        final CodeGenerator cg = new CodeGenerator();

        // Manually review the code
        final String code = cg.generateCode(this.entries);
        System.out.print("GENERATED CODE:\n" + code + "\n");

        if (!this.isWindows()) {
            final String filePath = AppTest.tempFilePath("test.java");
            Assert.assertTrue(cg.generateAndSave(this.entries, filePath));

            Process pr = null;

            try {
                pr = Runtime.getRuntime().exec("javac " + filePath);
                pr.waitFor();
            } catch (final Exception ex) {
                ex.printStackTrace();
                Assert.fail();
            }

            Assert.assertTrue(pr.exitValue() == 0);
        }
    }

    public void testValidFileName() {
        final CodeGenerator cg = new CodeGenerator();

        final String invalid = AppTest.tempFilePath("invalid.txt");
        Assert.assertFalse(cg.generateAndSave(this.entries, invalid));

        String valid = AppTest.tempFilePath("valid");
        Assert.assertTrue(cg.generateAndSave(this.entries, valid));

        valid += ".java";
        Assert.assertTrue(cg.generateAndSave(this.entries, valid));
    }

    public void testEmptyAndNullLayoutEntry() {
        final CodeGenerator cg = new CodeGenerator();
        Assert.assertTrue(cg.generateCode(null).length() == 0);
        Assert.assertTrue(cg.generateCode(new Vector<LayoutEntry>()).length() == 0);
    }

    private boolean isWindows() {
        final String os = System.getProperty("os.name").toLowerCase();
        return os.indexOf("win") >= 0;
    }

    private void init() {
        this.entries = new Vector<>();
        this.entries.add(new LayoutEntry(JButton.class));
        this.entries.add(new LayoutEntry(JLabel.class));
        this.entries.add(new LayoutEntry(JCheckBox.class));
        this.entries.add(new LayoutEntry(JButton.class));

        this.entries.get(0).getAttributes().get(0)
        .setAttributeValue("\"THIS IS A STRING. THERE ARE \"MANY'like_it\"\"");
    }
}
