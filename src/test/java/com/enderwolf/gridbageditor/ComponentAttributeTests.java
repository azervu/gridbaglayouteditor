package com.enderwolf.gridbageditor;

import java.io.InvalidClassException;

import junit.framework.Assert;
import junit.framework.TestCase;

public class ComponentAttributeTests extends TestCase {
    public void testValidTypes() {
        boolean anyThrow = false;

        try {
            new ComponentAttribute("Test", Integer.class, "0");
            new ComponentAttribute("Test", Double.class, "0");
            new ComponentAttribute("Test", Float.class, "0");
            new ComponentAttribute("Test", String.class, "huehuehuehue");
        } catch (final InvalidClassException ex) {
            anyThrow = true;
        }

        Assert.assertFalse(anyThrow);
    }

    public void testInvalidType() {
        boolean anyThrow = false;

        try {
            new ComponentAttribute("Test", Byte.class, "0");
        } catch (final InvalidClassException ex) {
            anyThrow = true;
        }

        Assert.assertTrue(anyThrow);
    }

    public void testValidIntegerValue() {
        ComponentAttribute ca = null;

        try {
            ca = new ComponentAttribute("Number", Integer.class, "0");
        } catch (final InvalidClassException ex) {
            Assert.fail();
        }

        Assert.assertNotNull(ca);
        Assert.assertTrue(ca.validTypeValue("45"));
        Assert.assertTrue(ca.validTypeValue("-45"));
        Assert.assertTrue(ca.validTypeValue(""));
        Assert.assertFalse(ca.validTypeValue("0.54"));
        Assert.assertFalse(ca.validTypeValue("--45"));
        Assert.assertFalse(ca.validTypeValue("45-"));
        Assert.assertFalse(ca.validTypeValue("hei"));
        Assert.assertFalse(ca.validTypeValue("-"));
    }

    public void testValidDoubleValue() {
        ComponentAttribute ca = null;

        try {
            ca = new ComponentAttribute("Number", Double.class, "0");
        } catch (final InvalidClassException ex) {
            Assert.fail();
        }

        Assert.assertNotNull(ca);
        Assert.assertTrue(ca.validTypeValue("0.5454"));
        Assert.assertTrue(ca.validTypeValue("5454"));
        Assert.assertTrue(ca.validTypeValue("-5454"));
        Assert.assertFalse(ca.validTypeValue("0."));
        Assert.assertFalse(ca.validTypeValue("-"));
        Assert.assertFalse(ca.validTypeValue(".554"));
        Assert.assertFalse(ca.validTypeValue("."));
    }
}
