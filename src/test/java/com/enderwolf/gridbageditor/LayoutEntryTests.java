package com.enderwolf.gridbageditor;

import java.awt.Component;
import java.util.List;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JTextField;

import junit.framework.Assert;
import junit.framework.TestCase;

public class LayoutEntryTests extends TestCase {
    public void testNonEmptyAttributeSet() {
        final Vector<Class<?>> types = GridBagModel.getValidComponentTypes();
        for (final Class<?> type : types) {
            final LayoutEntry le = new LayoutEntry((Class<? extends Component>) type);

            List<ComponentAttribute> attrs;
            attrs = le.getAttributes();

            Assert.assertNotNull(attrs);
            Assert.assertFalse(attrs.isEmpty());
        }
    }

    /**
     * Ensure that when the target-class of the LayoutEntry is changed, that the
     * existing values are retained when the target-class is assigned back to
     * the original type.
     */
    public void testTargetPersistentValues() {
        final LayoutEntry le = new LayoutEntry(JTextField.class);
        List<ComponentAttribute> attrs;

        // Alter the original attributes
        final String origText = "this is my text. there are many like it, etc";
        final String origColumn = "1337";

        attrs = le.getAttributes();

        // No values has been set, assert the defaults are not being assigned
        Assert.assertFalse(attrs.get(0).getAttributeValue().equals(origColumn));
        Assert.assertFalse(attrs.get(1).getAttributeValue().equals(origText));

        attrs.get(0).setAttributeValue(origColumn);
        attrs.get(1).setAttributeValue(origText);

        // The values HAVE been assigned, assert that the values actually
        // changed.
        Assert.assertTrue(attrs.get(0).getAttributeValue().equals(origColumn));
        Assert.assertTrue(attrs.get(1).getAttributeValue().equals(origText));

        // Alter the target class
        le.setTarget(JLabel.class);

        // Labels have fewer attributes than TextFields
        Assert.assertTrue(le.getAttributes().size() != attrs.size());
        le.getAttributes().get(0).setAttributeValue("LABEL TEXT");

        // Ensure that the previous text (idx 1) has not been altered
        Assert.assertTrue(attrs.get(1).getAttributeValue().equals(origText));

        // Change back to JTextField
        le.setTarget(JTextField.class);

        // The values should persist
        Assert.assertEquals(le.getAttributes().get(0).getAttributeValue(), attrs.get(0).getAttributeValue());
        Assert.assertEquals(le.getAttributes().get(1).getAttributeValue(), attrs.get(1).getAttributeValue());
    }
}
