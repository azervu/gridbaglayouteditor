package com.enderwolf.gridbageditor;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JLabel;

import junit.framework.Assert;
import junit.framework.TestCase;

public class GridBagModelTests extends TestCase {
    public void testValueGetSet() {
        final GridBagModel gbm = new GridBagModel();

        final AnchorItem ai = AnchorItem.intToAnchorItem(GridBagConstraints.CENTER);
        final FillItem fi = FillItem.intToFillItem(GridBagConstraints.BOTH);
        final Insets insets = new Insets(0, 0, 0, 0);

        gbm.newEntry();
        gbm.setValueAt(JLabel.class, 0, 0);
        gbm.setValueAt(ai, 0, 1);
        gbm.setValueAt(fi, 0, 2);
        gbm.setValueAt(1, 0, 3);
        gbm.setValueAt(2, 0, 4);
        gbm.setValueAt(3, 0, 5);
        gbm.setValueAt(4, 0, 6);
        gbm.setValueAt(insets, 0, 7);
        gbm.setValueAt(5, 0, 8);
        gbm.setValueAt(6, 0, 9);
        gbm.setValueAt(0.5, 0, 10);
        gbm.setValueAt(0.9, 0, 11);

        Assert.assertEquals(JLabel.class, gbm.getValueAt(0, 0));
        Assert.assertEquals(ai, gbm.getValueAt(0, 1));
        Assert.assertEquals(fi, gbm.getValueAt(0, 2));
        Assert.assertEquals(1, gbm.getValueAt(0, 3));
        Assert.assertEquals(2, gbm.getValueAt(0, 4));
        Assert.assertEquals(3, gbm.getValueAt(0, 5));
        Assert.assertEquals(4, gbm.getValueAt(0, 6));
        Assert.assertEquals(insets, gbm.getValueAt(0, 7));
        Assert.assertEquals(5, gbm.getValueAt(0, 8));
        Assert.assertEquals(6, gbm.getValueAt(0, 9));
        Assert.assertEquals(0.5, gbm.getValueAt(0, 10));
        Assert.assertEquals(0.9, gbm.getValueAt(0, 11));
    }

    public void testLoad() {
        final String filePath = AppTest.tempFilePath("gbm.ser");

        // Save the dummy instance to disk
        final GridBagModel saved = this.createDummyModel();
        saved.saveData(filePath);

        // Load it back in
        final GridBagModel loaded = new GridBagModel();
        loaded.loadData(filePath);

        Assert.assertEquals(saved.getRowCount(), loaded.getRowCount());

        // Assert that all the elements are equal
        for (int i = 0; i < 12; i++) {
            final Object objSaved = saved.getValueAt(0, 0);
            final Object objLoaded = loaded.getValueAt(0, 0);
            Assert.assertTrue(objSaved.equals(objLoaded));
        }
    }

    private GridBagModel createDummyModel() {
        final GridBagModel gbm = new GridBagModel();

        final AnchorItem ai = AnchorItem.intToAnchorItem(GridBagConstraints.CENTER);
        final FillItem fi = FillItem.intToFillItem(GridBagConstraints.BOTH);
        final Insets insets = new Insets(0, 0, 0, 0);

        gbm.newEntry();
        gbm.setValueAt(JLabel.class, 0, 0);
        gbm.setValueAt(ai, 0, 1);
        gbm.setValueAt(fi, 0, 2);
        gbm.setValueAt(1, 0, 3);
        gbm.setValueAt(1, 0, 4);
        gbm.setValueAt(1, 0, 5);
        gbm.setValueAt(1, 0, 6);
        gbm.setValueAt(insets, 0, 7);
        gbm.setValueAt(2, 0, 8);
        gbm.setValueAt(2, 0, 9);
        gbm.setValueAt(0.5, 0, 10);
        gbm.setValueAt(0.5, 0, 11);

        return gbm;
    }
}
