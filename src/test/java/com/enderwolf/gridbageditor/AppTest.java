package com.enderwolf.gridbageditor;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {
    /**
     * Create the test case
     *
     * @param testName
     *            name of the test case
     */
    public AppTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        final TestSuite suite = new TestSuite();
        suite.addTestSuite(GridBagModelTests.class);
        suite.addTestSuite(CodeGeneratorTests.class);
        suite.addTestSuite(ComponentAttributeTests.class);
        suite.addTestSuite(LayoutEntryTests.class);
        return suite;
    }

    /**
     * Get the absolute path to a temporary file.
     *
     * @param fileName
     *            The name of the file itself
     * @return The absolute path to a file named 'fileName' inside the temporary
     *         directory of the system.
     */
    public static String tempFilePath(String fileName) {
        String tmpDir = System.getProperty("java.io.tmpdir");
        if (tmpDir.lastIndexOf('/') != (tmpDir.length() - 1)) {
            tmpDir += "/";
        }

        return tmpDir + fileName;
    }

}
