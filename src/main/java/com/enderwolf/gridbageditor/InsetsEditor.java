/**
 *
 */
package com.enderwolf.gridbageditor;

import java.awt.Component;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.logging.Logger;

import javax.swing.AbstractCellEditor;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableCellEditor;

/**
 * @author !Tulingen
 *
 */
public class InsetsEditor extends AbstractCellEditor implements TableCellEditor, ChangeListener {
    private static final long serialVersionUID = -4364149221974144596L;

    private static final String TOP = "TOP";
    private static final String LEFT = "LEFT";
    private static final String BOTTOM = "BOTTOM";
    private static final String RIGHT = "RIGHT";

    private static final String[] DIRECTIONS = {
        InsetsEditor.TOP, InsetsEditor.LEFT, InsetsEditor.BOTTOM, InsetsEditor.RIGHT
    };

    private Insets value;
    private final JPanel base;
    private boolean selected = false;

    private final JSpinner[] spinners;

    /**
     * Default constructor
     */
    public InsetsEditor() {
        this.base = new JPanel();
        this.base.setLayout(new GridLayout(1, 4));
        this.base.setBorder(null);

        this.spinners = new JSpinner[InsetsEditor.DIRECTIONS.length];

        for (int i = 0; i < InsetsEditor.DIRECTIONS.length; i++) {
            final SpinnerNumberModel model = new SpinnerNumberModel();
            model.setMinimum(0);

            final JSpinner spinner = new JSpinner(model);
            spinner.setName(InsetsEditor.DIRECTIONS[i]);
            spinner.addChangeListener(this);
            spinner.setBorder(null);

            this.base.add(spinner);
            this.spinners[i] = spinner;
        }
    }
    
    /**
     * Sets the spinners display value to that of value
     * @param value
     */
    private void updateSpinners(Insets value) {
        this.value = value;
        
        this.spinners[0].setValue(this.value.top);
        this.spinners[1].setValue(this.value.left);
        this.spinners[2].setValue(this.value.bottom);
        this.spinners[3].setValue(this.value.right);
    }
    
    /**
     * Returns this editors current value
     */
    @Override
    public Object getCellEditorValue() {
        this.selected = false;
        return this.value;
    }

    /**
     * Returns this editors visual part if already selected, double click effect.
     */
    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        this.updateSpinners((Insets) value);

        if (isSelected && this.selected) {
            return this.base;
        } else if (isSelected && !this.selected) {
            this.selected = true;
        } else {
            this.selected = false;
        }

        return null;
    }

    /**
     * Listener for changes in value of the spinners.
     */
    @Override
    public void stateChanged(ChangeEvent e) {
        final JSpinner from = (JSpinner) e.getSource();

        switch (from.getName()) {
            case InsetsEditor.TOP:
                this.value.top = ((Integer) from.getValue());
                break;

            case InsetsEditor.LEFT:
                this.value.left = ((Integer) from.getValue());
                break;

            case InsetsEditor.BOTTOM:
                this.value.bottom = ((Integer) from.getValue());
                break;

            case InsetsEditor.RIGHT:
                this.value.right = ((Integer) from.getValue());
                break;

            default:
                Logger.getGlobal().severe("how the hell?");
                break;
        }
    }
}
