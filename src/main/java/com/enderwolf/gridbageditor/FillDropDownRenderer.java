/**
 *
 */
package com.enderwolf.gridbageditor;

import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 * @author !Tulingen
 *
 */
public class FillDropDownRenderer extends JLabel implements ListCellRenderer<FillItem> {
    private static final long serialVersionUID = 6194266623892969443L;

    public FillDropDownRenderer() {
        this.setOpaque(true);
    }

    @Override
    public Component getListCellRendererComponent(
            JList<? extends FillItem> list,
            FillItem value,
            int index,
            boolean isSelected,
            boolean cellHasFocus) {

        if (isSelected) {
            this.setBackground(list.getSelectionBackground());
            this.setForeground(list.getSelectionForeground());
        } else {
            this.setBackground(list.getBackground());
            this.setForeground(list.getForeground());
        }

        this.setIcon(new ImageIcon(this.getClass().getResource(value.getImagePath())));
        this.setText(value.getDescription());
        this.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 2));
        
        return this;
    }
}
