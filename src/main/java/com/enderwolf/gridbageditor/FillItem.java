package com.enderwolf.gridbageditor;

import java.awt.GridBagConstraints;
import java.util.Vector;

public class FillItem {

    private static final int[] FILL_LIST = {
        GridBagConstraints.NONE,
        GridBagConstraints.HORIZONTAL,
        GridBagConstraints.VERTICAL,
        GridBagConstraints.BOTH
    };

    private static final String[] FILL_LIST_NAME = {
        "None", "Horizontal", "Vertical", "Both"
    };

    private static final String[] FILL_LIST_IMAGE = {
        "NONE", "HORIZONTAL", "VERTICAL", "BOTH"
    };

    private static final String[] FILL_LIST_CODE = {
        "GridBagConstraints.NONE",
        "GridBagConstraints.HORIZONTAL",
        "GridBagConstraints.VERTICAL",
        "GridBagConstraints.BOTH"
    };

    private static final String IMAGE_ROOT_DIR = "images";

    private static final Vector<FillItem> VALID_FILL;

    private final int fillValue;
    private final String description;
    private final String imagePath;
    private final String code;

    private FillItem(int fillValue, String description, String imagePath, String code) {
        this.fillValue = fillValue;
        this.description = description;
        this.imagePath = imagePath;
        this.code = code;
    }

    public static Vector<FillItem> getValidFills() {
        return FillItem.VALID_FILL;
    }

    public static FillItem intToFillItem(int refrence) {
        for (final FillItem item : FillItem.VALID_FILL) {
            if (item.getFillValue() == refrence) {
                return item;
            }
        }

        return null;
    }

    /**
     * @return the fillValue
     */
    public int getFillValue() {
        return this.fillValue;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * @return the imagePath
     */
    public String getImagePath() {
        return this.imagePath;
    }

    /**
     * @return the code that this object represent
     */
    public String getCode() {
        return this.code;
    }

    @Override
    public String toString() {
        return this.description;
    }

    static {
        VALID_FILL = new Vector<>();

        for (int i = 0; i < FillItem.FILL_LIST.length; i++) {
            final FillItem item = new FillItem(FillItem.FILL_LIST[i], FillItem.FILL_LIST_NAME[i],
                    FillItem.IMAGE_ROOT_DIR + "/" + FillItem.FILL_LIST_IMAGE[i] + ".png", FillItem.FILL_LIST_CODE[i]);

            FillItem.VALID_FILL.add(item);
        }
    }
}
