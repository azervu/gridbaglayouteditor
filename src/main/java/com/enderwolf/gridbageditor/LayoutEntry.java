package com.enderwolf.gridbageditor;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.io.InvalidClassException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * A LayoutEntry contains (a) a "target" class, subtype of Component and (b) a
 * GridBagConstraints-instance that defines the constraints of the
 * to-be-generated instance of said target class.
 */
public class LayoutEntry implements Serializable {
    private static final long serialVersionUID = -8113710903138849418L;

    private Class<? extends Component> target;
    private GridBagConstraints parameters;

    /**
     * All supported attributes of all supported Component-types are created
     * during construction to preserve the defined states even if the user
     * changes the type of the LayoutEntry.
     */
    private Map<Class<?>, List<ComponentAttribute>> attributeMap;

    /**
     * Sole constructor of LayoutEntry. A target Component-class must be
     * defined. The LayoutEntry will during code generation be used to generate
     * code that instantiates an instance of 'target'.
     *
     * @param target
     *            The type of Component this entry defines
     */
    public LayoutEntry(Class<? extends Component> target) {
        this.mapAttributes();

        this.setTarget(target);
        this.setParameters(new GridBagConstraints());
    }

    /**
     * Retrieve the class this instance defines.
     *
     * @return the target
     */
    public Class<? extends Component> getTarget() {
        return this.target;
    }

    /**
     * Get the bare class-name of the target. If the target is
     * "java.awt.JLabel", only "JLabel" is returned.
     *
     * @return The bare name of the target class.
     * @see Class#getSimpleName()
     */
    public String getTargetClassName() {
        return this.target.getSimpleName();
    }

    /**
     * Define the target Component-class of this LayoutEntry. This entry will be
     * generated into a defined instance of this target class. Custom attributes
     * for this type will be automatically created.
     *
     * @param target
     *            the target class
     */
    public void setTarget(Class<? extends Component> target) {
        this.target = target;
    }

    /**
     * Retrieve the GridBagConstraints that have been user-defined in the editor
     * to apply to the to-be-generated instance of 'target'.
     *
     * @return the parameters
     */
    public GridBagConstraints getParameters() {
        return this.parameters;
    }

    /**
     * Define the GridBagConstraints for the Component this instance refers to.
     *
     * @param parameters
     *            The new GridBagConstraints to eventually apply to the target.
     */
    public void setParameters(GridBagConstraints parameters) {
        this.parameters = parameters;
    }

    /**
     * Retrieve the list of attributes related to the current target class. The
     * attributes persist throughout target-change.
     *
     * @return List containing the defined attributes of the current target. If
     *         no attributes are defined, an empty List is returned.
     */
    public List<ComponentAttribute> getAttributes() {
        if (this.attributeMap.containsKey(this.getTarget())) {
            return this.attributeMap.get(this.getTarget());
        }

        return new ArrayList<ComponentAttribute>();
    }

    /**
     * Add all assignable attributes in "target" to "this.attributes".
     */
    private void mapAttributes() {
        this.attributeMap = new HashMap<>();
        List<ComponentAttribute> attr;

        try {
            attr = new ArrayList<>();
            attr.add(new ComponentAttribute("Rows", Integer.class, "1"));
            attr.add(new ComponentAttribute("Columns", Integer.class, "10"));
            attr.add(new ComponentAttribute("Text", String.class, ""));
            this.attributeMap.put(JTextArea.class, attr);

            attr = new ArrayList<>();
            attr.add(new ComponentAttribute("Columns", Integer.class, "10"));
            attr.add(new ComponentAttribute("Text", String.class, ""));
            this.attributeMap.put(JTextField.class, attr);

            attr = new ArrayList<>();
            attr.add(new ComponentAttribute("Text", String.class, ""));
            this.attributeMap.put(JButton.class, attr);

            attr = new ArrayList<>();
            attr.add(new ComponentAttribute("Text", String.class, ""));
            this.attributeMap.put(JLabel.class, attr);
        } catch (final InvalidClassException ice) {
            Logger.getGlobal().log(Level.WARNING, "Unable to generate attribute map.", ice);
        }

        final Vector<Class<?>> gbmTypes = GridBagModel.getValidComponentTypes();
        for (final Class<?> type : gbmTypes) {
            if (!this.attributeMap.containsKey(type)) {
                Logger.getGlobal().log(Level.WARNING,
                        "GridBagModel-supported Type " + type.getName() + " is not defined in attributeMap");
            }
        }
    }
}
