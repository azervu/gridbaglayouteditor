/**
 *
 */
package com.enderwolf.gridbageditor;

import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * Renderer of the anchor-column in the editor.
 *
 * @see AnchorItem
 * @author !Tulingen
 */
public class AnchorRenderer extends DefaultTableCellRenderer {
    private static final long serialVersionUID = 4407317710751823370L;
    
    @Override
    public Component getTableCellRendererComponent(
            JTable table,
            Object value,
            boolean isSelected,
            boolean hasFocus,
            int row,
            int column) {

        final AnchorItem item = (AnchorItem) value;

        if (isSelected) {
            this.setBackground(table.getSelectionBackground());
            this.setForeground(table.getSelectionForeground());
        } else {
            this.setBackground(table.getBackground());
            this.setForeground(table.getForeground());
        }

        this.setIcon(new ImageIcon(this.getClass().getResource(item.getImagePath())));
        this.setText(item.getDescription());
        this.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 2));
        
        return this;
    }

}
