package com.enderwolf.gridbageditor;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

/**
 * AttributeEditorWindow is a window that, given a LayoutEntry, displays the
 * attributes of said LayoutEntry and allows editing of these attributes.
 */
public class AttributeEditorWindow extends JFrame {
    private static final long serialVersionUID = -7460100722098069134L;
    private final LayoutEntry layoutEntry;

    /**
     * Sole constructor for AttributeEditorWindow. The window contains
     * Components to alter the state of the attributes of the LayoutEntry, given
     * that the LayoutEntry's target contains any editable attributes.
     *
     * @param layout
     *            The layout to alter the attributes of.
     */
    public AttributeEditorWindow(LayoutEntry layout) {
        super(layout.getTargetClassName());

        this.layoutEntry = layout;

        if (!layout.getAttributes().isEmpty()) {
            this.requestFocus();
            this.setVisible(true);
            this.createLayout();
            this.pack();
            this.setMinimumSize(this.getSize());
        } else {
            JOptionPane.showMessageDialog(this, Messages.getString("AttributeEditorWindow.noattr"));
        }
    }

    /**
     * Create the layout containing a JLabel and a JTextField for each attribute
     * given the assigned LayoutEntry.
     */
    private void createLayout() {
        final List<ComponentAttribute> attrs = this.layoutEntry.getAttributes();

        final JPanel panel = new JPanel(new GridBagLayout());
        panel.setMinimumSize(new Dimension(500, 300));
        this.add(panel);

        final GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridy = 0;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;

        for (final ComponentAttribute ca : attrs) {
            final JLabel label = new JLabel();
            label.setText(ca.getAttributeName());
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.gridx = 0;
            gbc.anchor = GridBagConstraints.WEST;
            gbc.insets = new Insets(0, 0, 0, 100);
            panel.add(label, gbc);

            final JTextField textField = new JTextField();
            ((AbstractDocument) textField.getDocument()).setDocumentFilter(new AttributeTextFilter(ca));
            textField.setText(ca.getAttributeValue());
            textField.setColumns(15);
            gbc.fill = GridBagConstraints.BOTH;
            gbc.gridx = 1;
            gbc.anchor = GridBagConstraints.CENTER;
            gbc.insets = new Insets(0, 0, 0, 0);
            panel.add(textField, gbc);

            gbc.gridy++;
        }
    }

    /**
     * AttributeTextFilter listens for changes in the Document attached to a
     * JTextField related to a ComponentAttribute. It ensures that the changes
     * made to the JTextField are valid and commits the changes if they are
     * valid.
     */
    private class AttributeTextFilter extends DocumentFilter {
        ComponentAttribute attribute;

        /**
         * Sole constructor for AttributeTextFilter.
         *
         * @param attr
         *            The attribute the Document will alter.
         */
        public AttributeTextFilter(ComponentAttribute attr) {
            this.attribute = attr;
        }

        /**
         * Create the full text of the document if this change was blindly
         * accepted and verify that it is a valid value for the attribute. An
         * error dialog is shown if the verification fails.
         *
         * @see DocumentFilter#insertString(javax.swing.text.DocumentFilter.FilterBypass,
         *      int, String, javax.swing.text.AttributeSet)
         */
        @Override
        public void insertString(FilterBypass fb, int offset, String string, AttributeSet aset)
                throws BadLocationException {
            final int preLen = offset;
            final int postLen = fb.getDocument().getLength() - offset;

            final String pre = fb.getDocument().getText(0, preLen);
            final String post = postLen == 0 ? "" : fb.getDocument().getText(offset, postLen);
            final String full = pre + string + post;

            if (this.attribute.validTypeValue(full)) {
                this.attribute.setAttributeValue(full);
                super.insertString(fb, offset, string, aset);
            } else {
                final String msg = Messages.getString("AttributeEditorWindow.inval");
                JOptionPane.showMessageDialog(AttributeEditorWindow.this, msg);
            }
        }

        /**
         * Create the full text of the document if this change was blindly
         * accepted and verify that it is a valid value for the attribute. An
         * error dialog is shown if the verification fails.
         *
         * @see DocumentFilter#replace(javax.swing.text.DocumentFilter.FilterBypass,
         *      int, int, String, javax.swing.text.AttributeSet)
         */
        @Override
        public void replace(FilterBypass fb, int offset, int len, String text, AttributeSet aset)
                throws BadLocationException {
            final int preLen = offset;
            final int postLen = fb.getDocument().getLength() - offset - len;

            final String pre = fb.getDocument().getText(0, preLen);
            final String post = postLen == 0 ? "" : fb.getDocument().getText(offset + len, postLen);
            final String full = pre + text + post;

            if (this.attribute.validTypeValue(full)) {
                this.attribute.setAttributeValue(full);
                super.replace(fb, offset, len, text, aset);
            } else {
                final String msg = Messages.getString("AttributeEditorWindow.inval");
                JOptionPane.showMessageDialog(AttributeEditorWindow.this, msg);
            }
        }
    }
}
