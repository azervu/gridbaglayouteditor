package com.enderwolf.gridbageditor;

import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class ComponentRenderer extends DefaultTableCellRenderer {
    private static final long serialVersionUID = 914571234656L;
    
    @Override
    public Component getTableCellRendererComponent(
            JTable table,
            Object value,
            boolean isSelected,
            boolean hasFocus,
            int row,
            int column) {

        final Class<?> item = (Class<?>) value;

        if (isSelected) {
            this.setBackground(table.getSelectionBackground());
            this.setForeground(table.getSelectionForeground());
        } else {
            this.setBackground(table.getBackground());
            this.setForeground(table.getForeground());
        }

        this.setText(item.getSimpleName());
        this.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 2));
        
        return this;
    }
}
