package com.enderwolf.gridbageditor;

import java.io.InvalidClassException;
import java.io.Serializable;

/**
 * ComponentAttribute is an intermediate step between user defined values for
 * uncommon attributes found in Component subtypes and the code generated. The
 * ComponentAttribute is unaware of the class the attribute refers to.
 *
 * This class only supports simple, immediate types for the custom attributes -
 * Strings, integers, doubles, etc. Another requirement is that the attribute in
 * the related Component-class must be assignable with a set-method.
 */
public class ComponentAttribute implements Serializable {
    private static final long serialVersionUID = 2347123491824912L;

    private final String attributeName;
    private final Class<?> attributeType;
    private String attributeValue;
    private final String defaultValue;

    /**
     * Default constructor.
     *
     * @param name
     *            The name of the attribute. There *must* exist a method in the
     *            target class that can be constructed with "set" + name which
     *            takes a sole parameter of type 'type'.
     * @param type
     *            The type of this attribute. Must be of type int, double, float
     *            or String.
     * @param defaultValue
     *            The default value in String form.:w
     * @throws InvalidClassException
     */
    public ComponentAttribute(String name, Class<?> type, String defaultValue) throws InvalidClassException {
        if (type != String.class && type != Integer.class && type != Double.class && type != Float.class) {
            throw new InvalidClassException("ComponentAttribute only supports String, Integer, Double or Float. "
                    + "Unsupported type " + type.getSimpleName() + " given.");
        }

        this.attributeName = name;
        this.attributeType = type;
        this.attributeValue = defaultValue;
        this.defaultValue = defaultValue;
    }

    /**
     * Get the display-name of the attribute name.
     *
     * @return The display name for this attribute.
     */
    public String getAttributeName() {
        return this.attributeName;
    }

    /**
     * Get the type of this attribute.
     *
     * @return The type of the attribute instance refers to.
     */
    public Class<?> getAttributeType() {
        return this.attributeType;
    }

    /**
     * Set the value of the attribute this instance refers to.
     *
     * @param value
     *            The value on string-form.
     */
    public void setAttributeValue(String value) {
        this.attributeValue = value;
    }

    /**
     * Get the String-value of the value assigned to this attribute.
     *
     * @return The defined value assigned to this instance.
     */
    public String getAttributeValue() {
        return this.attributeValue;
    }

    /**
     * Get the default value assigned to this attribute.
     *
     * @return The default value for this attribute.
     */
    public String getDefaultValue() {
        return this.defaultValue;
    }

    /**
     * Check if the passed value is a valid value given the attribute type.
     *
     * @param value
     *            The value to check if is valid.
     * @return True if the value is valid, false otherwise.
     */
    public boolean validTypeValue(String value) {
        // Empty strings are OK - the default value will be used.
        if (value.isEmpty()) {
            return true;
        }

        if (this.attributeType == String.class) {
            return true;
        } else if (this.attributeType == Integer.class) {
            return value.matches("(-?[0-9]+|[0-9]*)");
        } else if (this.attributeType == Double.class || this.attributeType == Float.class) {
            return value.matches("-?[0-9]+(\\.[0-9]+)?");
        }

        /* never reached */
        return true;
    }

    /**
     * Get code that assigns the value to a variable.
     *
     * @param variableName
     *            The name of the variable to assign the attribute to.
     * @param indentLevel
     *            The number of tab characters to insert.
     * @return A string on the form "\tvariable.setAttribute("value");\n".
     */
    public String getAssignmentCode(String variableName, int indentLevel) {
        final StringBuilder sb = new StringBuilder();

        for (int i = 0; i < indentLevel; i++) {
            sb.append("\t");
        }

        sb.append(variableName);
        sb.append(".set");
        sb.append(this.attributeName.substring(0, 1).toUpperCase());
        sb.append(this.attributeName.substring(1));
        sb.append('(');

        if (this.attributeType == String.class) {
            sb.append('"');
            sb.append(this.attributeValue.replace("\"", "\\\""));
            sb.append('"');
        } else {
            if (!this.attributeValue.isEmpty()) {
                sb.append(this.attributeValue);
            } else {
                sb.append(this.defaultValue);
            }
        }

        sb.append(");\n");

        return sb.toString();
    }
}
