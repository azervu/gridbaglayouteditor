package com.enderwolf.gridbageditor;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class to generate an executable java program from a set of LayoutEntry
 * objects. The generated program does not do anything but display a set of
 * elements on screen.
 */
public class CodeGenerator {
    /**
     * The source code is built into this buffer.
     */
    StringBuilder buffer;

    /**
     * The LayoutEntries are assigned variable names and are stored in this map.
     */
    Map<String, LayoutEntry> varMap;

    private String lastError = "";
    private String className = "";

    public CodeGenerator() {
        this.varMap = new HashMap<String, LayoutEntry>();
        this.className = "MainClass";
    }

    /**
     * Generates the code given the layout entries and saves it to file.
     *
     * @param layouts
     *            The layouts to be created inside the window.
     * @param filePath
     *            The absolute path to the file to be created.
     * @return True if the file did not exist and the code was successfully
     *         written to file. If false is returned,
     *         CodeGenerator#getLastError() will return a description of what
     *         went wrong.
     * @see CodeGenerator#generateCode(List)
     * @see CodeGenerator#getLastError()
     */
    public boolean generateAndSave(List<LayoutEntry> layouts, String filePath) {
        final String translatedFilePath = this.translatePathName(filePath);

        if (translatedFilePath == null) {
            Logger.getGlobal().warning("Invalid file name");
            this.lastError = Messages.getString("CodeGenerator.errorInvalidFileName");
            return false;
        }

        final File file = new File(translatedFilePath);

        if (file.exists() && file.isDirectory()) {
            Logger.getGlobal().warning("Selected file is an existing directory.");
            this.lastError = Messages.getString("CodeGenerator.errorDirSelected");
            return false;
        }

        final String code = this.generateCode(layouts);

        if (code.length() == 0) {
            Logger.getGlobal().warning("No code generated.");
            this.lastError = Messages.getString("CodeGenerator.errorNoLayoutsDefined");
            return false;
        }

        try {
            final BufferedWriter bw = new BufferedWriter(new FileWriter(file));
            bw.write(code);
            bw.flush();
            bw.close();
        } catch (final IOException ioe) {
            Logger.getGlobal().log(Level.WARNING, Messages.getString("CodeGenerator.errorFileWriteFailed"), ioe);
            this.lastError = Messages.getString("CodeGenerator.errorFileWriteFailed");
            this.lastError += "\n\n'" + ioe.getLocalizedMessage() + "'";
            return false;
        }

        return true;
    }

    /**
     * Generate an executable java class that creates a window populated with a
     * set of elements in a GridBagLayout.
     *
     * @param layouts
     *            The layouts to be created inside the window.
     * @return A valid java-program in String form, an empty string if 'layouts'
     *         is null or empty.
     */
    public String generateCode(List<LayoutEntry> layouts) {
        if (layouts == null || layouts.isEmpty()) {
            return "";
        }

        this.buffer = new StringBuilder();

        this.assignVariableNames(layouts);

        this.importClasses();
        this.declareClassBegin();
        this.declareMemberVariables();
        this.defineConstructor();
        this.defineMainMethod();
        this.declareClassEnd();

        return this.buffer.toString();
    }

    public String getLastError() {
        return this.lastError;
    }

    /**
     * Import each Component class within the 'varMap'-map exactly once.
     */
    private void importClasses() {
        // Import universally required classes
        this.buffer.append("import javax.swing.JFrame;\n");
        this.buffer.append("import java.awt.GridBagLayout;\n");
        this.buffer.append("import java.awt.GridBagConstraints;\n");
        this.buffer.append("import javax.swing.JPanel;\n");
        this.buffer.append("\n");

        // Only import each class once
        final Set<String> set = new HashSet<>();
        final Collection<LayoutEntry> layouts = this.varMap.values();

        for (final LayoutEntry le : layouts) {
            final String fullName = le.getTarget().getName();
            if (!set.contains(fullName)) {
                set.add(fullName);
            }
        }

        for (final String c : set) {
            this.buffer.append("import " + c + ";\n");
        }
    }

    /**
     * Creates variable names for each LayoutEntry and relates them in the
     * 'varMap'-map.
     *
     * @param layouts
     *            The layouts to be assigned variable names.
     */
    private void assignVariableNames(List<LayoutEntry> layouts) {
        final Map<String, Integer> classCount = new HashMap<String, Integer>();
        this.varMap.clear();

        for (final LayoutEntry le : layouts) {
            String cname = le.getTargetClassName().toLowerCase();

            // Ignore the leading "j".
            if (cname.charAt(0) == 'j') {
                cname = cname.substring(1);
            }

            if (!classCount.containsKey(cname)) {
                classCount.put(cname, new Integer(1));
            } else {
                classCount.put(cname, classCount.get(cname) + 1);
            }

            this.varMap.put(cname + classCount.get(cname), le);
        }
    }

    /**
     * Declare the start of the class.
     */
    private void declareClassBegin() {
        this.buffer.append(String.format("\npublic class %s extends JPanel {\n", this.className));
    }

    /**
     * Define the constructor of the generated code. The constructor
     * instantiates all defined Components and assigns them their proper
     * constraints.
     */
    private void defineConstructor() {
        this.buffer.append(String.format("\tpublic %s() {\n", this.className));
        this.buffer.append("\t\tsuper(new GridBagLayout());\n");
        this.buffer.append("\t\tgbc = new GridBagConstraints();\n\n");

        // Instantiate the variables, properly define the 'gbc' variable and add
        // them to 'panel'.
        final Iterator<Entry<String, LayoutEntry>> it = this.varMap.entrySet().iterator();
        while (it.hasNext()) {
            final Entry<String, LayoutEntry> pair = it.next();

            final LayoutEntry le = pair.getValue();
            final String targetClassName = le.getTargetClassName();
            final String varName = pair.getKey();

            final GridBagConstraints gbc = le.getParameters();
            final Insets inset = gbc.insets;

            this.buffer.append(String.format("\t\t%s = new %s();\n", varName, targetClassName));
            this.buffer.append(String.format("\t\tgbc.anchor = %s;\n", this.getAnchorForLayout(le)));
            this.buffer.append(String.format("\t\tgbc.fill = %s;\n", this.getFillForLayout(le)));

            this.buffer.append(String.format("\t\tgbc.insets.left = %d;\n", inset.left));
            this.buffer.append(String.format("\t\tgbc.insets.right = %d;\n", inset.right));
            this.buffer.append(String.format("\t\tgbc.insets.top = %d;\n", inset.top));
            this.buffer.append(String.format("\t\tgbc.insets.bottom = %d;\n", inset.bottom));

            this.buffer.append(String.format("\t\tgbc.gridx = %d;\n", gbc.gridx));
            this.buffer.append(String.format("\t\tgbc.gridy = %d;\n", gbc.gridy));
            this.buffer.append(String.format("\t\tgbc.gridwidth = %d;\n", gbc.gridwidth));
            this.buffer.append(String.format("\t\tgbc.gridheight = %d;\n", gbc.gridheight));
            this.buffer.append(String.format("\t\tgbc.ipadx = %d;\n", gbc.ipadx));
            this.buffer.append(String.format("\t\tgbc.ipady = %d;\n", gbc.ipady));

            this.buffer.append(String.format("\t\tgbc.weightx = %f;\n", gbc.weightx));
            this.buffer.append(String.format("\t\tgbc.weighty = %f;\n", gbc.weighty));

            final List<ComponentAttribute> attributes = le.getAttributes();
            for (final ComponentAttribute attr : attributes) {
                this.buffer.append(attr.getAssignmentCode(varName, 2));
            }

            this.buffer.append(String.format("\t\tadd(%s, gbc);\n", varName));
            this.buffer.append("\n");
        }

        this.buffer.append("\t}\n\n");
    }

    /**
     * Define the main method. The main method of the generated code
     * instantiates an instance of the generated class and adds it to a JFrame.
     */
    private void defineMainMethod() {
        // Method signature
        this.buffer.append("\tpublic void main(String args[]) {\n");

        // Create the window
        this.buffer.append("\t\tJFrame win = new JFrame();\n");
        this.buffer.append("\t\twin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);\n");
        this.buffer.append("\t\twin.setVisible(true);\n");
        this.buffer.append("\n");
        this.buffer.append(String.format("\t\t%s panel = new %s();\n", this.className, this.className));
        this.buffer.append("\t\twin.add(panel);\n");
        this.buffer.append("\n");

        // Closing bracket
        this.buffer.append("\t}\n");
    }

    /**
     * Insert the tail of the class.
     */
    private void declareClassEnd() {
        this.buffer.append("\n}");
    }

    /**
     * Declare the member variables. This includes all member variables, both
     * universally used (GridBagConstraints gbc) and dynamically defined
     * (Components derived from LayoutEntry-instances).
     */
    private void declareMemberVariables() {
        this.buffer.append("\tprivate GridBagConstraints gbc;\n\n");

        final Iterator<Entry<String, LayoutEntry>> it = this.varMap.entrySet().iterator();
        while (it.hasNext()) {
            final Entry<String, LayoutEntry> pair = it.next();

            final String targetClassName = pair.getValue().getTargetClassName();
            final String varName = pair.getKey();
            this.buffer.append(String.format("\t%s %s;\n", targetClassName, varName));
        }

        this.buffer.append("\n");
    }

    /**
     * Inserts ".java" at the end of the file path if it does not contain a
     * "last name". Also assigns 'className' to the name of the file excluding
     * the last name of the file.
     *
     * @param uncleanfilePath
     *            The absolute path the user selected file to write to.
     * @return The absolute path with ".java" appended if it does not exist,
     *         'null' if the filePath is invalid.
     */
    private String translatePathName(String uncleanfilePath) {
        // Windows support
        final String filePath = uncleanfilePath.replace('\\', '/');
        final String fileName = filePath.substring(filePath.lastIndexOf('/') + 1);

        if (!fileName.matches("(\\w*)(\\.java)?$")) {
            Logger.getGlobal().log(Level.WARNING, "Invalid filename: "+fileName+"\n");
            return null;
        }

        final int idx = fileName.lastIndexOf('.');
        if (idx == -1) {
            this.className = fileName;
            return filePath + ".java";
        }

        if (!"java".equals(fileName.substring(idx + 1))) {
            // If the last name is not ".java", return null.
            return null;
        }

        this.className = fileName.substring(0, idx);
        return filePath;
    }

    /**
     * Returns the full string form of the 'anchor' component of le's grid bag
     * constraints.
     *
     * @param le
     *            The LayoutEntry to retrieve the anchor from.
     * @return String on the form 'GridBagConstraints.ENUMVALUE'
     */
    private String getAnchorForLayout(LayoutEntry le) {
        final GridBagConstraints gbc = le.getParameters();

        final AnchorItem item = AnchorItem.intToAnchorItem(gbc.anchor);

        if (item != null) {
            return item.getCode();
        }

        return "0";
    }

    /**
     * Returns the full string form of the 'fill' component of le's grid bag
     * constraints.
     *
     * @param le
     *            The LayoutEntry to retrieve the fill from.
     * @return String on the form 'GridBagConstraints.ENUMVALUE'
     */
    private String getFillForLayout(LayoutEntry le) {
        final GridBagConstraints gbc = le.getParameters();

        final FillItem item = FillItem.intToFillItem(gbc.anchor);

        if (item != null) {
            return item.getCode();
        }

        return "0";
    }
}
