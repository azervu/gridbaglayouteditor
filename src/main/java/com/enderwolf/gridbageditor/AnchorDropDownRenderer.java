/**
 *
 */
package com.enderwolf.gridbageditor;

import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 * Renderer of the anchor-column-dropdown menu. The class uses an AnchorItem
 * instance (which contains the name of the image) to render an icon.
 *
 * @see AnchorItem
 * @author !Tulingen
 */
public class AnchorDropDownRenderer extends JLabel implements ListCellRenderer<AnchorItem> {
    private static final long serialVersionUID = 3358255407998265805L;

    public AnchorDropDownRenderer() {
        this.setOpaque(true);
    }

    @Override
    public Component getListCellRendererComponent(
            JList<? extends AnchorItem> list,
            AnchorItem value,
            int index,
            boolean isSelected,
            boolean cellHasFocus) {

        if (isSelected) {
            this.setBackground(list.getSelectionBackground());
            this.setForeground(list.getSelectionForeground());
        } else {
            this.setBackground(list.getBackground());
            this.setForeground(list.getForeground());
        }

        this.setIcon(new ImageIcon(this.getClass().getResource(value.getImagePath())));
        this.setText(value.getDescription());
        this.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 2));
        
        return this;
    }
}
