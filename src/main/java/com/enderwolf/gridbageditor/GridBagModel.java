package com.enderwolf.gridbageditor;

import java.awt.Component;
import java.awt.Insets;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.AbstractTableModel;

/**
 * The highest level model in the program. This class encapsulates a set of
 * LayoutEntry-instances and allows modification of the instances. The model
 * also got functionality for writing and saving the model to file.
 *
 * @see LayoutEntry
 * @author !Tulingen
 */
public class GridBagModel extends AbstractTableModel {

    private static final long serialVersionUID = -6858647036146459157L;

    private static final String[] COLUM_NAME = {
        Messages.getString("GridBagModel.colum0"),
        Messages.getString("GridBagModel.colum1"),
        Messages.getString("GridBagModel.colum2"),
        Messages.getString("GridBagModel.colum3"),
        Messages.getString("GridBagModel.colum4"),
        Messages.getString("GridBagModel.colum5"),
        Messages.getString("GridBagModel.colum6"),
        Messages.getString("GridBagModel.colum7"),
        Messages.getString("GridBagModel.colum8"),
        Messages.getString("GridBagModel.colum9"),
        Messages.getString("GridBagModel.colum10"),
        Messages.getString("GridBagModel.colum11")
    };

    private static final Class<?>[] COLUM_CLASS = {
        Class.class,
        AnchorItem.class,
        FillItem.class,
        Integer.class,
        Integer.class,
        Integer.class,
        Integer.class,
        Insets.class,
        Integer.class,
        Integer.class,
        Double.class,
        Double.class,
    };

    private static final Class<?>[] COMPONENT_CLASSES = {
        JTextField.class, JTextArea.class, JButton.class, JLabel.class,
    };

    private Vector<LayoutEntry> data = new Vector<>();

    public GridBagModel() {
        super();
    }

    /**
     * This method clears the existing LayoutEntry-list and notifies the table
     * the model is attached to (if any) that the table-data set has changed.
     */
    public void newTable() {
        this.data = new Vector<>();
        this.fireTableDataChanged();
    }

    /**
     * Creates a new, uninitialized LayoutEntry to the data set. The newly
     * created instance defaults to a JTextField.
     */
    public void newEntry() {
        this.data.add(new LayoutEntry(JTextField.class));
        this.fireTableDataChanged();
    }

    /**
     * Delete the selected (if any) rows from the table.
     * @param rows  The rows to be deleted.
     */
    public void deleteRows(int[] rows) {
        if (rows != null && rows.length > 0) {
            Arrays.sort(rows);

            for (int i = 0; i < rows.length; i++) {
                this.data.remove(rows[i] - i);
            }

            this.fireTableDataChanged();
        }
    }

    /**
     * Save the contents of the data-set to file.
     *
     * @param fileName
     *            The path (relative or absolute) to the file the serialized
     *            contents will be written to.
     */
    public void saveData(String fileName) {

        try (ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(fileName))) {
            output.writeObject(this.data);
        } catch (final IOException ioe) {
            Logger.getGlobal().log(Level.WARNING, "Error writing to file", ioe);
        }
    }

    /**
     * Load the contents of a previously serialized data-set from file.
     *
     * @param fileName
     *            The path (relative or absolute) to the file the serialzied
     *            contents will be read from.
     */
    @SuppressWarnings("unchecked")
    public void loadData(String fileName) {
        this.data = new Vector<>();

        try (ObjectInputStream input = new ObjectInputStream(new FileInputStream(fileName))) {
            this.data = (Vector<LayoutEntry>) input.readObject();
        } catch (final EOFException eofe) {
            Logger.getGlobal().log(Level.INFO, "End of file", eofe);
        } catch (final ClassNotFoundException cnfe) {
            Logger.getGlobal().log(Level.WARNING, "Casting to LayoutEntry from ObjectInputStream failes", cnfe);
        } catch (final IOException ioe) {
            Logger.getGlobal().log(Level.WARNING, "Error reading from file", ioe);
        }

        this.fireTableDataChanged();

    }

    /**
     * @return A list containing all defined layout entries.
     */
    public List<LayoutEntry> getLayoutEntries() {
        return this.data;
    }

    /**
     * @return The number of columns (read: attributes) in the model.
     */
    @Override
    public int getColumnCount() {
        return GridBagModel.COLUM_NAME.length;
    }

    /**
     * @return The number of defined entries in the data set.
     */
    @Override
    public int getRowCount() {
        return this.data.size();
    }

    /**
     * Get the value of a specific attribute in a specific row.
     *
     * @param rowIndex
     *            The index of the row. Must be greater than 0 and less than
     *            getRowCount().
     * @param columnIndex
     *            The index of the column. Must be greater than 0 and less than
     *            getColumnCount().
     * @return An instance of the getColumnClass(columnIndex), holding the value
     *         of the specified row.
     * @see GridBagModel#getColumnClass(int)
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (!this.isCellEditable(rowIndex, columnIndex)) {
            return null;
        }

        final LayoutEntry entry = this.data.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return entry.getTarget();

            case 1:
                return AnchorItem.intToAnchorItem(entry.getParameters().anchor);

            case 2:
                return FillItem.intToFillItem(entry.getParameters().fill);

            case 3:
                return entry.getParameters().gridwidth;

            case 4:
                return entry.getParameters().gridheight;

            case 5:
                return entry.getParameters().gridx;

            case 6:
                return entry.getParameters().gridy;

            case 7:
                return entry.getParameters().insets;

            case 8:
                return entry.getParameters().ipadx;

            case 9:
                return entry.getParameters().ipady;

            case 10:
                return entry.getParameters().weightx;

            case 11:
                return entry.getParameters().weighty;

            default:
                return null;
        }
    }

    /**
     * @return The type of the column at the specified index.
     */
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return GridBagModel.COLUM_CLASS[columnIndex];
    }

    /**
     * @return The name of the column at the specified index.
     */
    @Override
    public String getColumnName(int column) {
        return GridBagModel.COLUM_NAME[column];
    }

    /**
     * Only allow editing for defined attributes in existing rows.
     *
     * @return True if the cell exists, false otherwise.
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return rowIndex >= 0 && rowIndex < this.data.size() && columnIndex >= 0
                && columnIndex < GridBagModel.COLUM_NAME.length;
    }

    /**
     * Set a specific attribute in an entry.
     *
     * @param aValue
     *            An object of instance getColumnClass(columnIndex).
     * @param rowIndex
     *            The index of the entry to be altered.
     * @param columnIndex
     *            The index of the attribute to be assigned.
     * @see GridBagModel#getColumnClass(int)
     */
    @SuppressWarnings("unchecked")
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

        final LayoutEntry entry = this.data.get(rowIndex);
        switch (columnIndex) {
            case 0:
                entry.setTarget((Class<? extends Component>) aValue);
                break;

            case 1:
                entry.getParameters().anchor = ((AnchorItem) aValue).getAnchorValue();
                break;

            case 2:
                entry.getParameters().fill = ((FillItem) aValue).getFillValue();
                break;

            case 3:
                entry.getParameters().gridwidth = (int) aValue;
                break;

            case 4:
                entry.getParameters().gridheight = (int) aValue;
                break;

            case 5:
                entry.getParameters().gridx = (int) aValue;
                break;

            case 6:
                entry.getParameters().gridy = (int) aValue;
                break;

            case 7:
                entry.getParameters().insets = (Insets) aValue;
                break;

            case 8:
                entry.getParameters().ipadx = (int) aValue;
                break;

            case 9:
                entry.getParameters().ipady = (int) aValue;
                break;

            case 10:
                entry.getParameters().weightx = (double) aValue;
                break;

            case 11:
                entry.getParameters().weighty = (double) aValue;
                break;

            default:
                Logger.getGlobal().severe("Invalid index called in setValueAt: " + columnIndex);
                break;
        }

        this.fireTableCellUpdated(rowIndex, columnIndex);
    }

    /**
     * Get the list of component types supported by the editor.
     *
     * @return A vector containing the classes of the available Component-types.
     */
    public static Vector<Class<?>> getValidComponentTypes() {
        return new Vector<>(Arrays.asList(GridBagModel.COMPONENT_CLASSES));
    }
}
