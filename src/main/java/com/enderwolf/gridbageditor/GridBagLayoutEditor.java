package com.enderwolf.gridbageditor;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;

import com.alee.laf.WebLookAndFeel;

/**
 * GridBagLayoutEditor is the main entry point of the program. All it does is
 * instantiate the GridBagTable and GridBagModel classes and provide the GUI to
 * access them.
 *
 * @author Sindre
 */
public class GridBagLayoutEditor extends JFrame {
    private static final long serialVersionUID = -7563846767550797589L;

    private static final Logger LOGGER = Logger.getGlobal();
    private final GridBagModel model;
    private final JFileChooser fileChooser = new JFileChooser();
    private String filePath;
    private final GridBagTable table;

    /**
     * Class constructor that initializes the editor GUI.
     */
    private GridBagLayoutEditor() {
        super("GridBag layout editor");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setMinimumSize(new Dimension(800, 600));

        this.model = new GridBagModel();
        this.table = new GridBagTable(this.model);
        this.createTablePopup(this.table);
        
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        final JScrollPane scrollPane = new JScrollPane(this.table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        this.add(scrollPane, BorderLayout.CENTER);

        
        
        final EditorToolBar toolBar = new EditorToolBar();
        this.getContentPane().add(toolBar, BorderLayout.NORTH);

        final EditorMenuBar menuBar = new EditorMenuBar();
        this.setJMenuBar(menuBar);

        this.pack();
        this.setVisible(true);
    }

    /**
     * Create a pop-table listener on a GridBagTable. The popup allows the user
     * to edit the attributed of the selected LayoutEntry in the table.
     *
     * @param table
     *            The table to which we are adding a popup-menu.
     */
    private void createTablePopup(GridBagTable table) {
        final JPopupMenu popupMenu = new JPopupMenu();
        final JMenuItem item = new JMenuItem(Messages.getString("GridBagLayoutEditor.editAttr"));
        popupMenu.add(item);

        table.addMouseListener(new PopupClickListener(item, popupMenu));
    }

    /**
     * Open an AttributeEditorWindow to edit a LayoutEntry defined in a specific
     * row.
     *
     * @param selectedRow
     *            The row to edit the attributes of.
     */
    private void showAttributeEditWindow(int selectedRow) {
        final LayoutEntry le = this.model.getLayoutEntries().get(selectedRow);
        new AttributeEditorWindow(le);
    }

    /**
     * Saves the data in the model if it has been saved to or loaded from a
     * file, else it calls saveAs.
     */
    private void saveTable() {
        if (this.filePath != null) {
            this.model.saveData(this.filePath);
        } else {
            this.saveTableAs();
        }
    }

    /**
     * Presents a file chooser dialog, if the users selects a file it saves to
     * it.
     */
    private void saveTableAs() {
        this.fileChooser.setDialogTitle(Messages.getString("GridBagLeyoutEditor.saveAs"));
        this.fileChooser.setCurrentDirectory(new java.io.File("."));

        final int userSelection = this.fileChooser.showSaveDialog(this);
        if (userSelection == JFileChooser.APPROVE_OPTION) {
            this.filePath = this.fileChooser.getSelectedFile().getAbsolutePath();
            this.model.saveData(this.filePath);
        }
    }

    /**
     * Presents a file chooser dialog, if the users selects a file it loads from
     * it.
     */
    private void loadTable() {
        this.fileChooser.setDialogTitle(Messages.getString("GridBagLeyoutEditor.load"));
        this.fileChooser.setCurrentDirectory(new java.io.File("."));

        final int userSelection = this.fileChooser.showOpenDialog(this);
        if (userSelection == JFileChooser.APPROVE_OPTION) {
            this.filePath = this.fileChooser.getSelectedFile().getAbsolutePath();
            this.model.loadData(this.filePath);
        }
    }

    /**
     * Presents a file chooser dialog. The code based on the defined layout is
     * generated and saved to the user-selected file. Displays an error dialog
     * if an error occurred when generating the code or saving it to file.
     */
    private void saveCode() {
        this.fileChooser.setDialogTitle(Messages.getString("GridBagLayoutExitor.generateCode"));
        this.fileChooser.setCurrentDirectory(new java.io.File("."));

        final int userSelection = this.fileChooser.showSaveDialog(this);
        if (userSelection == JFileChooser.APPROVE_OPTION) {
            final String javaFile = this.fileChooser.getSelectedFile().getAbsolutePath();

            final CodeGenerator cg = new CodeGenerator();
            if (!cg.generateAndSave(this.model.getLayoutEntries(), javaFile)) {
                // Display an alert dialog
                JOptionPane.showMessageDialog(this, cg.getLastError());
            }
        }
    }

    /**
     * Display the about dialog.
     */
    private void displayAboutDialog() {
        JOptionPane.showMessageDialog(this, Messages.getString("GridBagLayoutEditor.aboutText"));
    }

    /**
     * Discards the old table data.
     */
    private void newTable() {
        this.model.newTable();
        this.filePath = null;
    }

    /**
     * Listener used for popup edit menu
     */
    private class PopupClickListener implements MouseListener {
        private final JMenuItem item;
        private final JPopupMenu popupMenu;

        PopupClickListener(JMenuItem item, JPopupMenu popupMenu) {
            this.item = item;
            this.popupMenu = popupMenu;
        }

        @Override
        public void mouseClicked(MouseEvent mouseEvent) {
            // Unused
        }

        @Override
        public void mousePressed(MouseEvent mouseEvent) {
            // Unused
        }

        @Override
        public void mouseEntered(MouseEvent mouseEvent) {
            // Unused
        }

        @Override
        public void mouseExited(MouseEvent mouseEvent) {
            // Unused
        }

        @Override
        public void mouseReleased(MouseEvent me) {
            // me.isPopupTrigger() never returns true - check manually for right
            // click instead.
            if (me.getButton() == 3) {
                // Remove previous listeners
                if (this.item.getActionListeners().length > 0) {
                    for (final ActionListener al : this.item.getActionListeners()) {
                        this.item.removeActionListener(al);
                    }
                }

                this.item.addActionListener(e -> GridBagLayoutEditor.this
                        .showAttributeEditWindow(GridBagLayoutEditor.this.table.rowAtPoint(me.getPoint())));
                this.popupMenu.show(me.getComponent(), me.getX(), me.getY());
            }
        }
    }

    /**
     * EditorToolBar encapsulates the buttons on the tool bar and their
     * callbacks.
     */
    private class EditorToolBar extends JToolBar {

        private static final long serialVersionUID = -6007474262129540646L;

        /**
         * Class constructor
         */
        public EditorToolBar() {

            final JButton saveButton = new JButton(Messages.getString("GridBagLeyoutEditor.save"));
            saveButton.addActionListener(e -> GridBagLayoutEditor.this.saveTable());
            this.add(saveButton);

            final JButton loadButton = new JButton(Messages.getString("GridBagLeyoutEditor.load"));
            loadButton.addActionListener(e -> GridBagLayoutEditor.this.loadTable());
            this.add(loadButton);

            final JButton newEntryButton = new JButton(Messages.getString("GridBagLeyoutEditor.newEntry"));
            newEntryButton.addActionListener(e -> GridBagLayoutEditor.this.model.newEntry());
            this.add(newEntryButton);

            final JButton delButton = new JButton(Messages.getString("GridBagLayoutEditor.delete"));
            delButton.addActionListener(e -> GridBagLayoutEditor.this.model.deleteRows(GridBagLayoutEditor.this.table
                    .getSelectedRows()));
            this.add(delButton);
        }

    }

    /**
     * EditorMenuBar encapsulates the menu item on the menu bar and their
     * callbacks.
     */
    private class EditorMenuBar extends JMenuBar {
        private static final long serialVersionUID = 7684384003934569665L;

        /**
         * Class constructor
         */
        public EditorMenuBar() {

            final JMenu fileMenu = new JMenu(Messages.getString("GridBagLeyoutEditor.file"));
            this.add(fileMenu);

            final JMenuItem menuItemNew = new JMenuItem(Messages.getString("GridBagLeyoutEditor.new"));
            menuItemNew.addActionListener(e -> GridBagLayoutEditor.this.newTable());
            fileMenu.add(menuItemNew);

            final JMenuItem menuItemLoad = new JMenuItem(Messages.getString("GridBagLeyoutEditor.load"));
            menuItemLoad.addActionListener(e -> GridBagLayoutEditor.this.loadTable());
            fileMenu.add(menuItemLoad);

            final JMenuItem menuItemSave = new JMenuItem(Messages.getString("GridBagLeyoutEditor.save"));
            menuItemSave.addActionListener(e -> GridBagLayoutEditor.this.saveTable());
            fileMenu.add(menuItemSave);

            final JMenuItem menuItemSaveAs = new JMenuItem(Messages.getString("GridBagLeyoutEditor.saveAs"));
            menuItemSaveAs.addActionListener(e -> GridBagLayoutEditor.this.saveTableAs());
            fileMenu.add(menuItemSaveAs);

            final JMenuItem menuItemGenerate = new JMenuItem(Messages.getString("GridBagLeyoutEditor.generateCode"));
            menuItemGenerate.addActionListener(e -> GridBagLayoutEditor.this.saveCode());
            fileMenu.add(menuItemGenerate);

            final JMenu helpMenu = new JMenu(Messages.getString("GridBagLeyoutEditor.help"));
            this.add(helpMenu);

            final JMenuItem mntmAbout = new JMenuItem(Messages.getString("GridBagLayoutEditor.about"));
            mntmAbout.addActionListener(e -> GridBagLayoutEditor.this.displayAboutDialog());
            helpMenu.add(mntmAbout);
        }
    }

    /**
     * Creates an instance of GridBagLayoutEditor.
     * @param args The arguments passed to the program from the command line.
     */
    public static void main(String[] args) {
        GridBagLayoutEditor.LOGGER.setLevel(Level.ALL);

        try (InputStream is = GridBagLayoutEditor.class.getResourceAsStream("config/logging.properties")) {
            LogManager.getLogManager().readConfiguration(is);
        } catch (final IOException ex) {
            GridBagLayoutEditor.LOGGER.setLevel(Level.INFO);
            GridBagLayoutEditor.LOGGER.log(Level.WARNING, "unable to load config file", ex);
        }

        WebLookAndFeel.install();
        new GridBagLayoutEditor();
    }
}
