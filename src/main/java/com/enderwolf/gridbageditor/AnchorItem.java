package com.enderwolf.gridbageditor;

import java.awt.GridBagConstraints;
import java.util.Vector;

/**
 * AnchorItem is used to provide auxilliary data to the 'anchor' attribute of a
 * GridBagConstraints-instance.
 *
 * AnchorItems can not be created externally. AnchorItem creates a list of valid
 * instances in the static constructor which can later be retrieved by providing
 * one of the valid values for GridBagConstraints#anchor.
 */
public class AnchorItem {

    /**
     * List of all the valid values that can be given to
     * GridBagConstraints#anchor.
     */
    private static final int[] ANCHOR_LIST = {
        GridBagConstraints.CENTER,
        GridBagConstraints.NORTH,
        GridBagConstraints.NORTHEAST,
        GridBagConstraints.EAST,
        GridBagConstraints.SOUTHEAST,
        GridBagConstraints.SOUTH,
        GridBagConstraints.SOUTHWEST,
        GridBagConstraints.WEST,
        GridBagConstraints.NORTHWEST,
        GridBagConstraints.PAGE_START,
        GridBagConstraints.PAGE_END,
        GridBagConstraints.LINE_START,
        GridBagConstraints.LINE_END,
        GridBagConstraints.FIRST_LINE_START,
        GridBagConstraints.FIRST_LINE_END,
        GridBagConstraints.LAST_LINE_START,
        GridBagConstraints.LAST_LINE_END,
        GridBagConstraints.BASELINE,
        GridBagConstraints.BASELINE_LEADING,
        GridBagConstraints.BASELINE_TRAILING,
        GridBagConstraints.ABOVE_BASELINE,
        GridBagConstraints.ABOVE_BASELINE_LEADING,
        GridBagConstraints.ABOVE_BASELINE_TRAILING,
        GridBagConstraints.BELOW_BASELINE,
        GridBagConstraints.BELOW_BASELINE_LEADING,
        GridBagConstraints.BELOW_BASELINE_TRAILING
    };

    /**
     * Mapping between "human readable" descriptive strings and ANCHOR_LIST.
     */
    private static final String[] ANCHOR_LIST_NAME = {
        "Center",
        "North",
        "Northeast",
        "East",
        "Southeast",
        "South",
        "Southwest",
        "West",
        "Northwest",
        "Page start",
        "Page end",
        "Line start",
        "Line end",
        "First line start",
        "First line end",
        "Last line start",
        "Last line end",
        "Baseline",
        "Baseline leading",
        "Baseline trailing",
        "Above baseline",
        "Above baseline leading",
        "Above baseline trailing",
        "Below baseline",
        "Below baseline leading",
        "Below baseline trailing"
    };

    private static final String[] ANCHOR_LIST_IMAGE = {
        "CENTER",
        "NORTH",
        "NORTHEAST",
        "EAST",
        "SOUTHEAST",
        "SOUTH",
        "SOUTHWEST",
        "WEST",
        "NORTHWEST",
        "PAGE_START",
        "PAGE_END",
        "LINE_START",
        "LINE_END",
        "FIRST_LINE_START",
        "FIRST_LINE_END",
        "LAST_LINE_START",
        "LAST_LINE_END",
        "BASELINE",
        "BASELINE_LEADING",
        "BASELINE_TRAILING",
        "ABOVE_BASELINE",
        "ABOVE_BASELINE_LEADING",
        "ABOVE_BASELINE_TRAILING",
        "BELOW_BASELINE",
        "BELOW_BASELINE_LEADING",
        "BELOW_BASELINE_TRAILING"
    };

    /**
     * When generating code, the contents of this array maps to the values of
     * this array maps to ANCHOR_LIST.
     */
    private static final String[] ANCHOR_LIST_CODE = {
        "GridBagConstraints.CENTER",
        "GridBagConstraints.NORTH",
        "GridBagConstraints.NORTHEAST",
        "GridBagConstraints.EAST",
        "GridBagConstraints.SOUTHEAST",
        "GridBagConstraints.SOUTH",
        "GridBagConstraints.SOUTHWEST",
        "GridBagConstraints.WEST",
        "GridBagConstraints.NORTHWEST",
        "GridBagConstraints.PAGE_START",
        "GridBagConstraints.PAGE_END",
        "GridBagConstraints.LINE_START",
        "GridBagConstraints.LINE_END",
        "GridBagConstraints.FIRST_LINE_START",
        "GridBagConstraints.FIRST_LINE_END",
        "GridBagConstraints.LAST_LINE_START",
        "GridBagConstraints.LAST_LINE_END",
        "GridBagConstraints.BASELINE",
        "GridBagConstraints.BASELINE_LEADING",
        "GridBagConstraints.BASELINE_TRAILING",
        "GridBagConstraints.ABOVE_BASELINE",
        "GridBagConstraints.ABOVE_BASELINE_LEADING",
        "GridBagConstraints.ABOVE_BASELINE_TRAILING",
        "GridBagConstraints.BELOW_BASELINE",
        "GridBagConstraints.BELOW_BASELINE_LEADING",
        "GridBagConstraints.BELOW_BASELINE_TRAILING"
    };

    private static final String IMAGE_ROOT_DIR = "images";

    /**
     * List of all the valid instances that exists. This list is populated in
     * the static constructor.
     */
    private static final Vector<AnchorItem> VALID_ANCHOR;

    private final int anchorValue;
    private final String description;
    private final String imagePath;
    private final String code;

    private AnchorItem(int anchorValue, String description, String imagePath, String code) {
        this.anchorValue = anchorValue;
        this.description = description;
        this.imagePath = imagePath;
        this.code = code;
    }

    /**
     * @return The complete list of valid AnchorItems.
     */
    public static Vector<AnchorItem> getValidAnchors() {
        return AnchorItem.VALID_ANCHOR;
    }

    /**
     * Retrieve the AnchorItem instance related to a specific GridBagConstraints
     * anchor value.
     *
     * @param refrence
     *            A valid value for GridBagConstrainst#anchor.
     * @return The AnchorItem instance mapped to this anchor-value if 'refrence'
     *         is a valid anchor-value - null otherwise.
     */
    public static AnchorItem intToAnchorItem(int refrence) {
        for (final AnchorItem a : AnchorItem.VALID_ANCHOR) {
            if (a.getAnchorValue() == refrence) {
                return a;
            }
        }

        return null;
    }

    /**
     * @return The GridBagConstraints#anchor value.
     */
    public int getAnchorValue() {
        return this.anchorValue;
    }

    /**
     * @return The description of the anchor value.
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * @return the image path
     */
    public String getImagePath() {
        return this.imagePath;
    }

    /**
     * @return the code that this object represent
     */
    public String getCode() {
        return this.code;
    }

    @Override
    public String toString() {
        return this.description;
    }

    /**
     * Construct a list of all the valid AnchorItem instances.
     */
    static {
        VALID_ANCHOR = new Vector<>();

        for (int i = 0; i < AnchorItem.ANCHOR_LIST.length; i++) {
            final AnchorItem item = new AnchorItem(AnchorItem.ANCHOR_LIST[i], AnchorItem.ANCHOR_LIST_NAME[i],
                    AnchorItem.IMAGE_ROOT_DIR + "/" + AnchorItem.ANCHOR_LIST_IMAGE[i] + ".png",
                    AnchorItem.ANCHOR_LIST_CODE[i]);

            AnchorItem.VALID_ANCHOR.add(item);
        }
    }
}
