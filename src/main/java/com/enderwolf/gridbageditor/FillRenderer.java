/**
 *
 */
package com.enderwolf.gridbageditor;

import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * @author !Tulingen
 *
 */
public class FillRenderer extends DefaultTableCellRenderer {
    private static final long serialVersionUID = 8198966620019793227L;
    
    @Override
    public Component getTableCellRendererComponent(
            JTable table,
            Object value,
            boolean isSelected,
            boolean hasFocus,
            int row,
            int column) {

        final FillItem item = (FillItem) value;

        if (isSelected) {
            this.setBackground(table.getSelectionBackground());
            this.setForeground(table.getSelectionForeground());
        } else {
            this.setBackground(table.getBackground());
            this.setForeground(table.getForeground());
        }

        this.setIcon(new ImageIcon(this.getClass().getResource(item.getImagePath())));
        this.setText(item.getDescription());
        this.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 2));
        
        return this;
    }

}
