package com.enderwolf.gridbageditor;

import java.awt.Dimension;
import java.awt.Insets;
import java.util.Arrays;

import javax.swing.JComboBox;
import javax.swing.JTable;

import com.alee.utils.swing.WebDefaultCellEditor;

/**
 * GridBagTable displays the defined layout entries on screen.
 *
 * @see LayoutEntry
 */
public class GridBagTable extends JTable {

    private static final long serialVersionUID = 7719911588206883078L;

    /**
     * The widths of the columns in the table.
     */
    private static final Integer[] COLUMN_WIDTH = {
        128, 96, 96, 80, 80, 80, 80, 144, 80, 80, 80, 80
    };

    public GridBagTable(GridBagModel gridBagModel) {
        super(gridBagModel);
        
        int sum = Arrays.asList(COLUMN_WIDTH).stream().mapToInt(e -> e).sum();
        
        this.setMinimumSize(new Dimension(sum, 480));
    }

    /**
     * Avoid concurrency problems by overriding initializeLocalVars() and
     * calling the super's method before instantiating own members variables.
     */
    @Override
    protected void initializeLocalVars() {
        super.initializeLocalVars();

        final JComboBox<Class<?>> componentCombo = new JComboBox<>(GridBagModel.getValidComponentTypes());
        componentCombo.setRenderer(new ComponentDropDownRenderer());
        this.setDefaultRenderer(Class.class, new ComponentRenderer());
        this.setDefaultEditor(Class.class, new WebDefaultCellEditor<>(componentCombo));

        final JComboBox<AnchorItem> anchorCombo = new JComboBox<>(AnchorItem.getValidAnchors());
        anchorCombo.setRenderer(new AnchorDropDownRenderer());
        this.setDefaultRenderer(AnchorItem.class, new AnchorRenderer());
        this.setDefaultEditor(AnchorItem.class, new WebDefaultCellEditor<>(anchorCombo));

        final JComboBox<FillItem> fillCombo = new JComboBox<>(FillItem.getValidFills());
        fillCombo.setRenderer(new FillDropDownRenderer());
        this.setDefaultRenderer(FillItem.class, new FillRenderer());
        this.setDefaultEditor(FillItem.class, new WebDefaultCellEditor<>(fillCombo));

        this.setDefaultRenderer(Insets.class, new InsetsRenderer());
        this.setDefaultEditor(Insets.class, new InsetsEditor());

        for (int i = 0; i < GridBagTable.COLUMN_WIDTH.length; i++) {
            this.getColumn(this.getModel().getColumnName(i)).setMinWidth(GridBagTable.COLUMN_WIDTH[i]);
        }
    }
}
