/**
 *
 */
package com.enderwolf.gridbageditor;

import java.awt.Component;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * @author !Tulingen
 *
 */
public class InsetsRenderer extends DefaultTableCellRenderer {
    private static final long serialVersionUID = -4760968408473855787L;
    
    @Override
    public Component getTableCellRendererComponent(
            JTable table,
            Object value,
            boolean isSelected,
            boolean hasFocus,
            int row,
            int column) {

        final Insets insets = (Insets) value;

        if (isSelected) {
            this.setBackground(table.getSelectionBackground());
            this.setForeground(table.getSelectionForeground());
        } else {
            this.setBackground(table.getBackground());
            this.setForeground(table.getForeground());
        }

        final StringBuilder text = new StringBuilder();
        text.append("T:");
        text.append(insets.top);
        text.append(" L:");
        text.append(insets.left);
        text.append(" B:");
        text.append(insets.bottom);
        text.append(" R:");
        text.append(insets.right);

        this.setText(text.toString());
        this.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 2));
        
        return this;
    }

}
