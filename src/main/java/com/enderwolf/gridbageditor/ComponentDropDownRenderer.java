/**
 *
 */
package com.enderwolf.gridbageditor;

import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 * @author !Tulingen
 *
 */
public class ComponentDropDownRenderer extends JLabel implements ListCellRenderer<Class<?>> {
    private static final long serialVersionUID = -6611191558216514619L;

    public ComponentDropDownRenderer() {
        this.setOpaque(true);
    }

    @Override
    public Component getListCellRendererComponent(
            JList<? extends Class<?>> list,
                    Class<?> value,
                    int index,
                    boolean isSelected,
                    boolean cellHasFocus) {

        if (isSelected) {
            this.setBackground(list.getSelectionBackground());
            this.setForeground(list.getSelectionForeground());
        } else {
            this.setBackground(list.getBackground());
            this.setForeground(list.getForeground());
        }

        final String fullname = value.getName();
        final String name = fullname.substring(fullname.lastIndexOf('.') + 1);

        this.setText(name);
        this.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 2));
        
        return this;
    }
}
