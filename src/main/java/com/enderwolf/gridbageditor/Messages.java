package com.enderwolf.gridbageditor;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This singleton provides global access to resources such as strings.
 */
public class Messages {
    private static final String DEFAULT_FILE = "com.enderwolf.gridbageditor.i18n.messages";
    private static ResourceBundle RESOURCE_BUNDLE;

    /**
     * Private constructor defeats instantiation
     */
    private Messages() {
    }

    /**
     * Provides a string in the relevant language
     *
     * @param key
     *            the key to the string loaded from i18n
     * @return the string loaded from i18n
     */
    public static String getString(String key) {
        try {
            return Messages.RESOURCE_BUNDLE.getString(key);
        } catch (final MissingResourceException e) {
            Logger.getGlobal().log(Level.WARNING, "String " + key + " has no translation for current lang", e);
            return '!' + key + '!';
        }
    }

    static {
        final Locale currentLocale = Locale.getDefault();

        try {
            Messages.RESOURCE_BUNDLE = ResourceBundle.getBundle(Messages.DEFAULT_FILE, currentLocale);
        } catch (final java.util.MissingResourceException e) {
            Logger.getGlobal().log(Level.INFO, "Using default lang", e);
            Messages.RESOURCE_BUNDLE = ResourceBundle.getBundle(Messages.DEFAULT_FILE);
        }
    }
}